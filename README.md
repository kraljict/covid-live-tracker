### This application is a live tracker of 19-nCoV(Covid-19). You have the option to view confirmed cases, infected cases, and deaths from the global scale down to every country in the world. It updates daily as it is fetched from an API.

# See Live Demo --> https://ncov-tracker.netlify.app/

<img src="covid_img.png" height="500" width="800">