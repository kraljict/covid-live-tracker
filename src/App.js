import React from 'react';
import Cards from './components/Cards/Cards';
import CountryPicker from './components/CountryPicker/CountryPicker';
import styles from './App.module.css';
import {fetchData} from './API';

class App extends React.Component {
    state = {
        data: {},
        country: '',
    }

   

    async componentDidMount ( ) {
        const fetchedData = await fetchData();

        this.setState({ data: fetchedData})
    }

    handleCountryChange = async (country) => {
        const fetchedData = await fetchData(country);

        this.setState({ data: fetchedData, country: country });
    }

    render() {
        const { data } = this.state;
        return (
            <div className={styles.container}>
                <h1>2019-nCoV (COVID-19) Live Tracker</h1>
                <Cards data={data} />
                <CountryPicker handleCountryChange={this.handleCountryChange} />
                
            </div>
        );
    }
}

export default App;